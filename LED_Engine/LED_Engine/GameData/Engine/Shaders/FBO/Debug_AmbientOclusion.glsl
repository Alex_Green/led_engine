#version 330
uniform sampler2D TextureUnit2;
uniform sampler2D TextureUnit3;
uniform sampler2D TextureUnit5;
//uniform sampler2D TextureUnit6;

//#include("SSAO.glsl")

in vec2 f_UV;

layout(location = 0) out vec4 FragColor;

void main()
{
	vec3 Position = texture(TextureUnit3, f_UV).rgb;
	
	vec2 NormalXY = texture(TextureUnit2, f_UV).rg;
	vec3 Normal = vec3(NormalXY, sqrt(1.0 - dot(NormalXY, NormalXY)));
	
	vec3 Ka = texture(TextureUnit5, f_UV).rgb;
	//float SSAO_Factor = SSAO(TextureUnit3, TextureUnit2, TextureUnit6, f_UV, Position, Normal);
	//FragColor = vec4(Ka * SSAO_Factor, 1.0); //FragColor = vec4(vec3(SSAO_Factor), 1.0);
	FragColor = vec4(Ka, 1.0);
}