#version 330
uniform sampler2D TextureUnit2;

in vec2 f_UV;

#include("Light.glsl")

layout(location = 0) out vec4 FragColor;

void main()
{
	vec2 NormalXY = texture(TextureUnit2, f_UV).rg;
	vec3 Normal = vec3(NormalXY, sqrt(1.0 - dot(NormalXY, NormalXY)));
	
	FragColor = vec4(Normal * 0.5 + 0.5, 1.0);
}